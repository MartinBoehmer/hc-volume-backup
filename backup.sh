#!/bin/bash

# Copyright 2019-2020 Martin Böhmer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

### Functions ###

function check_hc_error {
  hc_error=$( jq -r 'if .error then true else false end' <<< "$1" )
  if [ "$hc_error" == 'true' ]
  then
    hc_error_code=$( jq -r '.error.code' <<< "$1" )
    hc_error_msg=$( jq -r '.error.message' <<< "$1" )
    echo "Error ($hc_error_code): $hc_error_msg" 1>&2
    exit 1
  fi
}

function wait_hc_action {
  hc_action_data="$1"
  hc_action_id=$( jq -r '.action.id' <<< "$hc_action_data" )
  printf "Waiting for action to complete (ID=%s)" $hc_action_id
  for (( i=0; i<30; i++ ))
  do
    hc_action_status=$( jq -r '.action.status' <<< "$hc_action_data" )
    if [ "$hc_action_status" == "success" ]
    then
      printf "\n"
      return 0
    fi
    sleep 2s
    hc_action_data=$( \
     curl -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $hc_access_token" \
     "https://api.hetzner.cloud/v1/actions/$hc_action_id"
    )
    if [ $debug == "true" ] || [ $debug == "1" ]
    then
      echo "\n$hc_action_data"
    else
      printf '.'
    fi
    check_hc_error "$hc_action_data"
  done
  echo "Timeout for action (ID=$hc_action_id)"
  exit 1
}

### Backup script for Hetzner cloud volumes ###
echo "Hetzner Cloud volume backup"

# Read configuration
echo "Reading configuration from $1"
if [ "$1" == "" ]
then
  config_file="hc_backup.config"
else 
  config_file="$1"
fi
. $1

# Check, if encryption is enabled
if [ -f "$backup_key_file" ] || [ "$backup_key_derive_from" != "" ]
then
  backup_target_encrypt=true
else
  backup_target_encrypt=false
fi

# Debug info
if [ "$debug" = true ]
then
  echo "Using access token: $hc_access_token"
  echo "Encryption of target volumes: $backup_target_encrypt"
  echo "Encryption with key file: $backup_key_file"
  echo "Encryption with key derived from: $backup_key_derive_from"
fi

# Gather info about source volume
echo "Querying size of backup source volume (ID=$backup_source_volume_id)"
backup_source_volume_data=$( \
  curl -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $hc_access_token" \
  "https://api.hetzner.cloud/v1/volumes/$backup_source_volume_id"
)
if [ "$debug" = true ]
then
  echo "$backup_source_volume_data"
fi
check_hc_error "$backup_source_volume_data"
backup_source_volume_size=$( jq -r '.volume.size' <<< "$backup_source_volume_data" )
backup_source_volume_name=$( jq -r '.volume.name' <<< "$backup_source_volume_data" )
hc_server_id=$( jq -r '.volume.server' <<< "$backup_source_volume_data" )
echo "Size of source volume [GB]: $backup_source_volume_size"
echo "Name of source volume: $backup_source_volume_name"
echo "Source volume is attached to server ID: $hc_server_id"

# Create target volume
backup_target_volume_size=$((backup_source_volume_size + 1))
backup_timestamp=$( date +"%Y-%m-%d_%H-%M-%S" )
backup_target_volume_name="BKP_${backup_source_volume_id}_${backup_timestamp}"
echo "Creating target volume named $backup_target_volume_name of $backup_target_volume_size GB size"
backup_target_volume_data=$( curl -s -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $hc_access_token" \
  -d "{\"size\": $backup_target_volume_size, \"name\": \"$backup_target_volume_name\", \"server\": $hc_server_id, \"automount\": false, \"labels\": {\"backup\":\"$backup_source_volume_name\"} }" \
  'https://api.hetzner.cloud/v1/volumes' \
)
if [ "$debug" = true ]
then
  echo "$backup_target_volume_data"
fi
check_hc_error "$backup_target_volume_data"
wait_hc_action "$backup_target_volume_data"
backup_target_volume_device=$( jq -r '.volume.linux_device' <<< "$backup_target_volume_data" )
backup_target_volume_id=$( jq -r '.volume.id' <<< "$backup_target_volume_data" )
backup_target_volume_name=$( jq -r '.volume.name' <<< "$backup_target_volume_data" )
echo "Target volume created. ID: $backup_target_volume_id, device: $backup_target_volume_device"
backup_target_dir="$backup_target_volume_id-${backup_target_volume_name// /_}"
backup_target_path="$backup_target_basedir/$backup_target_dir"
printf "Waiting for device to become available"
for (( i=0; i<30; i++ ))
do
  sleep 2s
  printf "."
  if [[ -e "$backup_target_volume_device" ]]
  then
    break
  fi
done
printf "\n"

# Create LUKS target volume, if required
if [ "$backup_target_encrypt" = true ]
then
  if [ -f "$backup_key_file" ]
  then
    echo "Creating encrypted volume via LUKS with key file"
    # Format - key file
    sudo cryptsetup -q luksFormat "$backup_target_volume_device" --key-file "$backup_key_file"
    # Add second key, if required
    if [ "$backup_key_derive_from" != "" ]
    then
      echo "Adding additional derived key to volume"
      sudo /lib/cryptsetup/scripts/decrypt_derived "$backup_key_derive_from" | sudo cryptsetup -q luksAddKey "$backup_target_volume_device" --key-file "$backup_key_file"
    fi
    # Open
    sudo cryptsetup luksOpen "$backup_target_volume_device" "$backup_target_dir" --key-file "$backup_key_file"
  else
    echo "Creating encrypted volume with dervied key only"
    # Format - only derived key
    sudo /lib/cryptsetup/scripts/decrypt_derived "$backup_key_derive_from" | sudo cryptsetup luksFormat "$backup_target_volume_device"
    # Open - only dervied key
    sudo /lib/cryptsetup/scripts/decrypt_derived "$backup_key_derive_from" | sudo cryptsetup luksOpen "$backup_target_volume_device" "$backup_target_dir"
  fi
  backup_target_mapper="/dev/mapper/$backup_target_dir"
else
  backup_target_mapper="$backup_target_volume_device"
fi

# Format target volume
echo "Formatting backup volume"
sudo mkfs.ext4 "$backup_target_mapper"

# Mount target volume
echo "Backup volume mount point: $backup_target_path"
echo "Backup volume device/mapper: $backup_target_mapper"
sudo mkdir -p "$backup_target_path"
sudo mount -o discard,defaults "$backup_target_mapper" "$backup_target_path"
echo "Backup volume mounted"

# Backup
echo "Starting backup of: $backup_source_path"
if [ "$debug" = true ]
then
  sudo rsync -a --info=progress2 "$backup_source_path" "$backup_target_path"
else
  sudo rsync -a "$backup_source_path" "$backup_target_path"
fi

# Unmount target volume
sudo umount $backup_target_path
echo "Backup volume unmounted"
sudo rmdir $backup_target_path

# Close LUKS target volume, if required
if [ "$backup_target_encrypt" = true ]
then
  echo "Closing encrypted volume via LUKS"
  sudo cryptsetup luksClose "$backup_target_dir"
fi

# Detach target volume from server
echo "Detaching volume from machine"
hc_detach_data=$( \
 curl -s -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $hc_access_token" \
 "https://api.hetzner.cloud/v1/volumes/$backup_target_volume_id/actions/detach"
)
check_hc_error "$hc_detach_data"
wait_hc_action "$hc_detach_data"

# Cleanup old backups
echo "Querying current set of backups"
backup_volumes_data=$( \
  curl -s -H "Authorization: Bearer $hc_access_token" \
  "https://api.hetzner.cloud/v1/volumes?sort=created%3Aasc&label_selector=backup%3D$backup_source_volume_name"
)
if [ "$debug" = true ]
then
  echo "$backup_volumes_data"
fi
check_hc_error "$backup_volumes_data"
backup_volumes_id=( $(jq -r '.volumes[].id' <<< "$backup_volumes_data") )
backup_total_volumes=${#backup_volumes_id[@]}
echo "Found $backup_total_volumes backup volumes for $backup_source_volume_name ($backup_source_volume_id)"
backups_to_delete=$(( backup_total_volumes - backup_max_volumes ))
if [ "$backups_to_delete" -le 0 ]
then
  echo "Maxiumum number of $backup_max_volumes backups not exceeded. No deletion required."
else
  echo "Deleting last $backups_to_delete backup volumes"
  for (( i=0; i<$backups_to_delete; i++ ))
  do
    backup_volume_id=${backup_volumes_id[$i]}
    echo "Deleting backup volume ID $backup_volume_id"
    backup_delete_data=$( \
     curl -s -X DELETE -H "Authorization: Bearer $hc_access_token" \
     "https://api.hetzner.cloud/v1/volumes/$backup_volume_id"
    )
    check_hc_error "$backup_delete_data"
  done
fi

echo "Done."
exit 0
